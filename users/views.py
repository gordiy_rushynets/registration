from django.shortcuts import render
from .forms import RegistrationForm, UserLoginForm
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import PasswordResetForm
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages 
from django.contrib.auth.models import User
import re

from django.core.mail import EmailMessage
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site 
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.tokens import PasswordResetTokenGenerator

from django.core.exceptions import ValidationError


# Registration 
def sign_up(request):
	form = RegistrationForm(request.POST or None)
	if form.is_valid():
		user = form.save(commit=False)

		username = form.cleaned_data['username']
		password = form.cleaned_data['password1']
		password2 = form.cleaned_data['password2']
		email = form.cleaned_data['email']

		match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email)

		# check weather user was created before
		if User.objects.filter(username=username):
			messages.error(request, 'This username does not exist.')
		elif User.objects.filter(email=email) == email:
			messages.error(request, "This email does not exist.")
		# regex for email
		elif match == None:
			messages.error(request, "Please, enter correct email!")
		# Show error message if password1 != password2
		elif password != password2: 
			messages.error(request, "The two password fields didn't match.")
		else:
			user.is_active = False
			user.save()	
			# create elements email
			mail_subject = 'Account Activation Email'
			subject = 'Email Activation'
			current_site = get_current_site(request) # get url our site
			
			# create conformation letter for user 
			message = render_to_string('registration/acc_active_email.html',
				{
					'user': user,
					'domain': current_site.domain,
					'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
					'token': PasswordResetTokenGenerator().make_token(user)
				})
			to_email = email

			# create object email
			email_msg = EmailMessage(mail_subject, message, to=[to_email])
			# send message with conformation link
			email_msg.send()
			
			# send_mail(subject, message, from_email, to_list, fail_silently=True)
			return HttpResponse('Please confirm your email address to complete the registration')
	else:
		form = RegistrationForm(request.POST or None)

	args = {
		'form': form,
		}
	return render(request, 'layout_3/LTR/default/full/login_registration.html', args)


# Activate User
def activate(request, uid, token):
	try:
		uid = urlsafe_base64_decode(uid).decode()
		user = User.objects.get(pk=uid)
	except (TypeError, ValueError, OverflowError, User.DoesNotExist):
		user=None
	
	if user is not None and default_token_generator.check_token(user, token):
		user.is_active = True
		user.save()
		login(request, user)
		return HttpResponseRedirect('/sign_in/')
	else:
		return HttpResponse('Activation link is invalid!')


# Login User
def sign_in(request):
	form = UserLoginForm(request.POST or None) # load form

	# check if form is not empty and create user object
	if form.is_valid():
		user_obj = form.cleaned_data.get('user_obj')

		# Check if user are created before
		print(User.objects.get(username=user_obj))
		if User.objects.get(username=user_obj) != user_obj:
			messages.error(request, "This username does not exist")

		else:
			login(request, user_obj)
			return HttpResponseRedirect('/')

	args = {'form': form}
	return render(request, 'layout_3/LTR/default/full/login_advanced.html', args)
