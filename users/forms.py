from django import forms
from django.contrib.auth.models import User
from django.db.models import Q
from django.contrib.auth.forms import UserCreationForm


# Registration User
class RegistrationForm(UserCreationForm):
	username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username'}))
	email = forms.EmailField(required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Your email'}))
	password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Enter password'}))
	password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password confirmation'}))

	class Meta:
		model = User
		fields = (
            'username',
            'email',
            'password1',
            'password2'
        )

	# Function which save user's data to admin
	def save(self, commit=True):
		user = super(RegistrationForm, self).save(commit=False)
		user.email = self.cleaned_data['email']

		if commit:
			user.save()
		return user


# Custom SignIn Form. 
class UserLoginForm(forms.Form):
	# Created query which must check 2 element: Username or Email when user login
	query = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username / Email'}))
	password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))

	def clean(self, *args, **kwargs): # creation user query set
		query = self.cleaned_data.get('query')
		password = self.cleaned_data.get('password')
		user_qs_final = User.objects.filter(
				Q(username__iexact=query)|
				Q(email__iexact=query)
			).distinct()

		if not user_qs_final.exists() and user_qs_final.count != 1: # If username or email is uncorrect we show error
			raise forms.ValidationError("Invalid credentials - user does not exist")
		
		user_obj = user_qs_final.first()

		if not user_obj.check_password(password): # Check if password is correct
			raise forms.ValidationError('Credentials are not correct')

		self.cleaned_data['user_obj'] = user_obj
		return super(UserLoginForm, self).clean(*args, **kwargs)


# Custom password reset Form

class PasswordReset(forms.Form):
	email = forms.EmailField(required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Your email'}))

	class Meta:
		model = User 
		fields = ['username' , 'email']
