from django.urls import path, re_path
from . import views


urlpatterns = [
    path('sign_up/', views.sign_up, name='sign_up'),
    path('sign_in/', views.sign_in, name='sign_in'),
    #re_path(r'^(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
     #   views.activate, name='activate'),
    path('<uid>/<token>/', views.activate, name='activate')
    ]