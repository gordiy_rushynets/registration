from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views


urlpatterns = [
	path('admin/', admin.site.urls),
	path('', include('mainpage.urls')),
	path('', include('users.urls')),
	path('', include('django.contrib.auth.urls')),
]
